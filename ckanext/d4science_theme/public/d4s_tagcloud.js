/*!
 * d4s_tagcloud.js
 * D4science Tag Cloud which using Tag Cloud Plugin for JQuery
 *
 * jquery.tagcloud.js
 * created by Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 */
loadTagCloudJS = function (elementIDtoCloud, rgb_start, rgb_end) {
	
	//console.log('start: '+rgb_start)
	//console.log('end: '+rgb_end)
	
	if(!rgb_start)
		rgb_start = '#C0C0C0';
	
	if(!rgb_end)
		rgb_end = '#000066';

	var script = document.createElement('script');
		script.onload = function() {
			/*console.log("TagCloud json loaded and ready");*/
   		$.fn.tagcloud.defaults = {
  			size: {start: 13, end: 20, unit: 'px'},
  			color: {start: rgb_start, end: rgb_end}
  		}
  		$('#'+elementIDtoCloud +' a').tagcloud();
	};
	
	script.type = "text/javascript";
	script.src = "jquery.tagcloud.js";
	document.getElementsByTagName('head')[0].appendChild(script);
}

loadCloud = function (elementIDtoCloud, rgb_start, rgb_end) {

	if(!window.jQuery){
  	 var script = document.createElement('script');
		script.onload = function() {
  		/*console.log("JQuery loaded and ready");*/
   	loadTagCloudJS(elementIDtoCloud,rgb_start,rgb_end);
	};
	
   script.type = "text/javascript";
   script.src = "https://code.jquery.com/jquery-1.11.0.min.js";
   document.getElementsByTagName('head')[0].appendChild(script);
  
	}else {
		loadTagCloudJS(elementIDtoCloud,rgb_start,rgb_end);
	}

	/*SHUFFLE TAGS*/
	var cloud = document.querySelector('#'+elementIDtoCloud);
	if (cloud == null)
		return;
	for (var i = cloud.children.length; i >= 0; i--) {
    	cloud.appendChild(cloud.children[Math.random() * i | 0]);
	}
}