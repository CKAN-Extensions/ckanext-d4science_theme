import datetime
import logging
import os
import tempfile
import csv

from icproxycontroller import NAMESPACE_ID_LABEL

log = logging.getLogger(__name__)

CATALINA_HOME = 'CATALINA_HOME'
temp_dir = None
namespaces_dir = None
NAMESPACES_DIR_NAME = "namespaces_for_catalogue"
NAMESPACES_CACHE_FILENAME = "Namespaces_Catalogue_Categories.csv"

# Created by Francesco Mangiacrapa
# francesco.mangiacrapa@isti.cnr.it
# ISTI-CNR Pisa (ITALY)


# D4S_Cache_Controller
class D4S_Cache_Controller():
    namespaces_cache_path = None
    __scheduler = None

    def __init__(self):
        """ Virtually private constructor. """
        log.debug("__init__ D4S_Cache_Controller")
        self._check_cache()

    def _check_cache(self):

        if self.namespaces_cache_path is None:
            self.init_temp_dir()
            self.namespaces_cache_path = os.path.join(namespaces_dir, NAMESPACES_CACHE_FILENAME)
            log.info("The namespaces cache is located at: %s" % self.namespaces_cache_path)

        if not os.path.exists(self.namespaces_cache_path):
            log.info("File does not exists creating it")
            try:
                with open(self.namespaces_cache_path, mode='w') as namespaces_file:
                    csv.writer(namespaces_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    log.info("Cache created at %s" % self.namespaces_cache_path)
            except Exception as e:
                print(e)

    ''' Write the list of dictionary with namespaces'''
    def write_namespaces(self, namespace_list_of_dict):
        # Insert Data
        with open(self.namespaces_cache_path, 'w') as namespaces_file:
            writer = csv.writer(namespaces_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow([NAMESPACE_ID_LABEL, 'name', 'title', 'description'])
            for namespace_dict in namespace_list_of_dict:
                #print("namespace %s" % namespace_dict)
                writer.writerow([namespace_dict[NAMESPACE_ID_LABEL], namespace_dict['name'], namespace_dict['title'], namespace_dict['description']])

        log.info("Inserted %d namespaces in the Cache" % len(namespace_list_of_dict))

    '''Returns the list of dictionary with namespaces'''
    def read_namespaces(self):
        # Read Data
        namespace_list_of_dict = []
        try:
            with open(self.namespaces_cache_path, 'r') as namespaces_file:
                reader = csv.DictReader(namespaces_file)
                for row in reader:
                    #print("read namespace %s" % row)
                    namespace_list_of_dict.append(dict(row))

            log.debug("from Cache returning namespace_list_of_dict %s: " % namespace_list_of_dict)
            log.info("from Cache read namespace_list_of_dict with %d item/s " % len(namespace_list_of_dict))
            return namespace_list_of_dict
        except Exception as e:
            print(e)

        log.info("no namespace in the Cache returning empty list of dict")
        return namespace_list_of_dict

    @property
    def get_namespaces_cache_path(self):
        return self.namespaces_cache_path

    @classmethod
    def init_temp_dir(cls):
        global temp_dir
        global NAMESPACES_DIR_NAME
        global namespaces_dir
        try:
            temp_dir = str(os.environ[CATALINA_HOME])
            temp_dir = os.path.join(temp_dir, "temp")
        except KeyError as error:
            log.error("No environment variable for: %s" % CATALINA_HOME)

        if temp_dir is None:
            temp_dir = tempfile.gettempdir()  # using system tmp dir

        log.debug("Temp dir is: %s" % temp_dir)

        namespaces_dir = os.path.join(temp_dir, NAMESPACES_DIR_NAME)

        if not os.path.exists(namespaces_dir):
            os.makedirs(namespaces_dir)



