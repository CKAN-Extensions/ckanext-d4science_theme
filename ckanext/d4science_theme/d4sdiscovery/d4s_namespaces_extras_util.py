import logging
import collections
from d4s_extras import D4S_Extras

CATEGORY = 'category'
NOCATEOGORY = 'nocategory'

log = logging.getLogger(__name__)

# Created by Francesco Mangiacrapa
# francesco.mangiacrapa@isti.cnr.it
# ISTI-CNR Pisa (ITALY)


# D4S_Namespaces_Extra_Util is used to get the extra fields indexed for D4Science namespaces 
# @param: namespace_dict is the namespace dict of D4Science namespaces (defined in the Generic Resource: "Namespaces Catalogue Categories")
# @param: extras is the dictionary of extra fields for a certain item
class D4S_Namespaces_Extra_Util():
    
    def get_extras_indexed_for_namespaces(self, namespace_dict, extras):
        extras_for_categories = collections.OrderedDict()

        # ADDING ALL EXTRAS WITH NAMESPACE
        for namespaceid in namespace_dict.keys():
            dict_extras = None
            nms = namespaceid + ":"
            #has_namespace_ref = None
            for key, value in extras:
                k = key
                v = value
                # print "key: " + k
                # print "value: " + v
                if k.startswith(nms):

                    if namespaceid not in extras_for_categories:
                        extras_for_categories[namespaceid] = collections.OrderedDict()

                    dict_extras = extras_for_categories[namespaceid]
                    log.debug("dict_extras %s "%dict_extras)

                    if (dict_extras is None) or (not dict_extras):
                        dict_extras = D4S_Extras(namespace_dict.get(namespaceid), [])
                        log.debug("dict_extras after init %s " % dict_extras)

                    #print ("dict_extras after init %s " % dict_extras)
                    log.debug("replacing namespace into key %s " % k +" with empty string")
                    nms = namespaceid + ":"
                    k = k.replace(nms, "")
                    dict_extras.append_extra(k, v)
                    extras_for_categories[namespaceid] = dict_extras
                    log.debug("adding d4s_extra: %s " % dict_extras+ " - to namespace id: %s" %namespaceid)
                    #has_namespace_ref = True
                    #break

        #ADDING ALL EXTRAS WITHOUT NAMESPACE
        for key, value in extras:
            k = key
            v = value

            has_namespace_ref = None
            for namespaceid in namespace_dict.keys():
                nms = namespaceid + ":"
                #IF KEY NOT STARTING WITH NAMESPACE
                if k.startswith(nms):
                    has_namespace_ref = True
                    log.debug("key: %s " % k + " - have namespace: %s" % nms)
                    break

            if has_namespace_ref is None:
                log.debug("key: %s " % k + " - have not namespace")
                if NOCATEOGORY not in extras_for_categories:
                    extras_for_categories[NOCATEOGORY] = collections.OrderedDict()

                dict_extras_no_cat = extras_for_categories[NOCATEOGORY]
                #print ("dict_extras_no_cat %s " % dict_extras_no_cat)

                if (dict_extras_no_cat is None) or (not dict_extras_no_cat):
                    dict_extras_no_cat = D4S_Extras(NOCATEOGORY, [])

                #print ("adding key: %s "%k+" - value: %s"%v)
                log.debug("NOCATEOGORY adding key: %s " % k + " - value: %s" % v)

                dict_extras_no_cat.append_extra(k, v)
                log.debug("dict_extras_no_cat %s " % dict_extras_no_cat)
                extras_for_categories[NOCATEOGORY] = dict_extras_no_cat
                log.debug("extras_for_categories NOCATEOGORY %s " % extras_for_categories)

        return extras_for_categories

