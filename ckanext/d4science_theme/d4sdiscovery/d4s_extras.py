# Created by Francesco Mangiacrapa
# francesco.mangiacrapa@isti.cnr.it
# ISTI-CNR Pisa (ITALY)

import logging
log = logging.getLogger(__name__)

class D4S_Extras():

    def __init__(self, category_dict={}, extras=[]):
        self._category = category_dict
        self._extras = extras

    def append_extra(self, k, v):
        #print ("self._extras: %s" %self._extras)
        if k is not None:
            self._extras.append({k:v})

    @property
    def category(self):
        return self._category

    @property
    def extras(self):
        return self._extras

    def __repr__(self):
        return 'category: %s'%self._category+' ' \
                'extras: %s'%self._extras

