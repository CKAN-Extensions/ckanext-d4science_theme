# Created by Francesco Mangiacrapa
# francesco.mangiacrapa@isti.cnr.it
# ISTI-CNR Pisa (ITALY)

#OrderedDict([(u'@id', u'extra_information'), (u'name', u'Extra Information'), (u'title', u'Extras'), (u'description', u'This section is about Extra(s)')]), u'contact': OrderedDict([(u'@id', u'contact'), (u'name', u'Contact'), (u'title', u'Contact Title'), (u'description', u'This section is about Contact(s)')]), u'developer_information': OrderedDict([(u'@id', u'developer_information'), (u'name', u'Developer'), (u'title', u'Developer Information'), (u'description', u'This section is about Developer(s)')])}

import logging
log = logging.getLogger(__name__)

class D4S_Namespaces():

    def __init__(self, id=None, name=None, title=None, description=None):
        self._id = id
        self._name = name
        self._title = title
        self._description = description

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def title(self):
        return self._title


    @property
    def description(self):
        return self._description

    def __repr__(self):
        return '{id: %s'%self.id+', ' \
                'name: %s'%self.name+ ', ' \
                'title: %s'%self.title+ ', ' \
                'description: %s'%self.description+ '}'
