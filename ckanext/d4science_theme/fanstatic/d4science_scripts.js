/* =====================================================
   JavaScript used by CKAN plugin: 'd4science_theme' 
   Created by Francesco Mangiacrapa ISTI-CNR Pisa, Italy
   ===================================================== */
   
   
CKAN_D4S_Breadcrumb_Manager = {
	
	breadcrumbShow : function (show) {
	
		var breadcrumb = document.getElementById('ckan-breadcrumb');
		console.log('breadcrumb is '+breadcrumb)
		if(breadcrumb){
			if(show){
				breadcrumb.style.display = 'block';
				this.organizationTreeShow(true);
			} else{ 
				breadcrumb.style.display = 'none';
				this.organizationTreeShow(false);
			}
		}
		
		//var elements = document.getElementsByTagName('a');
		//for(var i = 0, len = elements.length; i < len; i++) {
		//	elements[i].onclick = function () {
	    //  	//alert("You clicked an external link to: " + this.href);
	    //  	//window.parent.add_hide_breadcrumb_to_dom(false);
	   //   	this.add_hide_breadcrumb_to_dom(false);
	   // }
		//}
	},
	
	organizationTreeShow : function (show) {
		var trees = document.getElementsByClassName("hierarchy-tree-top");
		
		if (trees){
			for (i = 0; i < trees.length; i++) { 
				if(show){
					trees[i].style.display = 'block';
				} else{ 
					trees[i].style.display = 'none';
				}
    		}
		}
	},

	checkBreadcrumbShow : function () {
		
		var showBdc = this.getSessionStorageItem("showbreadcrumb")
		//console.log("showBdc is: "+showBdc)
		if(showBdc != undefined && showBdc=="false"){
			console.log("Show breadcrumb false");
			this.breadcrumbShow(false);
		}else{
			console.log("Show breadcrumb true");
			this.breadcrumbShow(true);
		}
	},


	setSessionStorageItem : function (item_key, item_value) {
	
		// Check browser support
		if (typeof(Storage) !== "undefined") {
	    	// Store
	    	sessionStorage.setItem(item_key, item_value);
	    	return true;
		} else {
	    	console.log("Sorry, your browser does not support Web Storage...");
	    	return false;
	    }
	},


	getSessionStorageItem : function (item_key) {
	
		// Check browser support
		if (typeof(Storage) !== "undefined") {
	    	// Store
	    	return sessionStorage.getItem(item_key);
		} else {
	    	console.log("Sorry, your browser does not support Web Storage...");
	    	return undefined;
	   }
	}

}


CKAN_D4S_Functions_Util = {
	
	getPosition : function(canvas, event){
	  var x = new Number();
	  var y = new Number();
	  try {
	  if (event.clientX != undefined && event.clientY != undefined)
	  {

		 x = event.clientX;
		 y = event.clientY;
	  }
	  else // Firefox method to get the position
	  {
		 x = event.clientX + document.body.scrollLeft +
		     document.documentElement.scrollLeft;
		 y = event.clientY + document.body.scrollTop +
		     document.documentElement.scrollTop;
	  }
	  x -= canvas.offsetLeft;
	  y -= canvas.offsetTop;
	  }catch (err) {
	  	//silent error
	  }
	  return '{"posX": "'+x+'", "posY": "'+y+'"}';
	},
	
	// When the user clicks on div, open the popup
	showPopupD4S : function(event, my_div, my_popup_left_position) {
		var popup = document.getElementById(my_div);
		var clickPosition = this.getPosition(my_div, event)
		var myPosition = JSON.parse(clickPosition);
		this.closePopups(my_div);
		// When the user clicks anywhere outside of the modal, close it
		/*window.onclick = function(event) {
	    if (event.target != popup) {
	        popup.style.display = "none";
	    }
		}*/
		popup.classList.toggle("show");
	
		if(my_popup_left_position){
			popup.style.left = my_popup_left_position;
		}
		else if (myPosition.posX){
			popup.style.left = myPosition.posX + "px";
		}
	},
	
	closePopups : function ($target) {
		var popups = document.getElementsByClassName('popuptext');
    	for (i = 0; i < popups.length; i++) {
      	if (popups[i].getAttribute('id') != $target) {
        		popups[i].classList.remove('show');
       	}	
    	}
	},
	
	checkURL : function (url) {
		//console.log('checking url: '+url)
		var regex = new RegExp('^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.|ftp:\/\/)+[^ "]+$');
		if (regex.test(url)) {
			return true;
	   }
	   return false;
	}

}

CKAN_D4S_HTMLMessage_Util = {
	
   postHeightToPortlet : function (selectedProduct, product) {
	 	var h = document.body.scrollHeight + "px";
	 	var p = "";
	 	var msg = "";
	 	//WORK AROUND IF TWO MESSAGES ARE SENT FROM A PAGE OF A PRODUCT
	 	//THE MESSAGE WITH 'NULL' PRODUCT IS SKIPPED
	 	//console.log("window.location.pathname? "+window.location.pathname);
	 	var pathArray = window.location.pathname.split('/');
	 	var productContext = "dataset";
	 	if(pathArray.length>1){
	 		//console.log("pathArray is: "+pathArray);     	
	 		var secondLevelLocation = pathArray[1]; //it is the second level location
	 		//console.log("secondLevelLocation is: "+secondLevelLocation);
	 		//console.log("h is: "+h);     	
	 		if(secondLevelLocation == productContext){ //is it product context?
	 			if(product !== 'undefined' && product !== null){
	 				p = product;
	 				//console.log("product selected is: "+p);
	 			}else{
	 				//console.log("product is null or undefined, passing only height");
	 				msg = "{\"height\": \""+h+"\"}"; 	
	 				//window.postMessage(msg,'*');
	 				this.postMessageToParentWindow(msg);
	 				return;
	 			}
	 		}
	 	}
	 	
	 	//msg = "{'height': '"+h+"', 'product': '"+p+"'}";
	 	msg = "{\"height\": \""+h+"\", \"product\": \""+p+"\"}"; 	
	 	//window.postMessage(msg,'*');
	 	//console.log("posting message in the window: "+msg);
	 	this.postMessageToParentWindow(msg);
 	},
 	
 	postMessageToParentWindow : function (msg) {
 		//window.postMessage(msg,'*');
 		//console.log("posting message in the window: "+msg);
 		if(window.parent!=null){
 			console.log("posting message in the parent window: "+msg);
 			window.parent.postMessage(msg,'*');
	 	}
	 	return;
	 }

}

CKAN_D4S_JSON_Util = {


  //ADDED by Francesco Mangiacrapa
  appendHTMLToElement : function(containerID, elementHTML){

     var divContainer = document.getElementById(containerID);
     divContainer.innerHTML = "";
     divContainer.appendChild(elementHTML);
  },

 //ADDED by Francesco Mangiacrapa
 jsonToHTML : function(containerID, cssClassToTable) {

    try
    {
       var jsonTxt = document.getElementById(containerID).innerHTML;
       var jsonObj = JSON.parse(jsonTxt);
       //console.log(jsonObj.length)

       if(jsonObj.length==undefined)
        jsonObj = [jsonObj]
        //console.log(jsonObj.length)

      // EXTRACT VALUE FOR HTML HEADER.
       var col = [];
       for (var i = 0; i < jsonObj.length; i++) {
        for (var key in jsonObj[i]) {
          //console.log('key json' +key)
          if (col.indexOf(key) === -1) {
            col.push(key);
          }
        }
       }

       // CREATE DYNAMIC TABLE.
       var table = document.createElement("table");
       var addDefaultCss = "json-to-html-table-column";
       if(cssClassToTable){
       	addDefaultCss = cssClassToTable;
       }
       try{
    	    table.classList.add(addDefaultCss);
       }catch(e){
    		  console.log('invalid css add', e);
       }

       // ADD JSON DATA TO THE TABLE AS ROWS.
       for (var i = 0; i < col.length; i++) {

          tr = table.insertRow(-1);
          var firstCell = tr.insertCell(-1);
          //firstCell.style.cssText="font-weight: bold; text-align: center; vertical-align: middle;";
          firstCell.innerHTML = col[i];
          for (var j = 0; j < jsonObj.length; j++) {
            var tabCell = tr.insertCell(-1);
            var theValue = jsonObj[j][col[i]];
            /*  console.log(theValue + ' is url? '+isUrl);*/
            if(CKAN_D4S_Functions_Util.checkURL(theValue)){
            	theValue = '<a target="_blank" href='+theValue+'>'+theValue+'</a>';
            }
            
            tabCell.innerHTML = theValue;
           }
       }

       // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
       this.appendHTMLToElement(containerID, table);

    }
    catch(e){
       console.log('invalid json', e);
    }
  }
}


//Task #8032
window.addEventListener("message",
 function (e) {
 
 		 var curr_loc = window.location.toString()
 		 var orgin = e.origin.toString()
 		 if(curr_loc.startsWith(orgin)){
 		  	//alert("ignoring message from myself");
      	return; 
 		 }
 		 //console.log("origin: "+e.data)
 		 if(e.data == null)
 		 	return;
 		 	
 		 var pMess = JSON.parse(e.data)
 		 //console.log(pMess.explore_vres_landing_page)
 		 window.linktogateway = pMess.explore_vres_landing_page;
 },false);
 