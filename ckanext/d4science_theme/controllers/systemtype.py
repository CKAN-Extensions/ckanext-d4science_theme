import logging
import ckan.plugins as p
from ckan.common import OrderedDict, _, g, c
import ckan.lib.search as search
import ckan.model as model
import ckan.logic as logic
import ckan.lib.maintain as maintain
import ckan.lib.base as base
import ckan.lib.helpers as h

from urllib import urlencode

from pylons.i18n import get_lang

import ckan.lib.base as base
import ckan.lib.helpers as h
import ckan.lib.maintain as maintain
import ckan.lib.navl.dictization_functions as dict_fns
import ckan.logic as logic
import ckan.lib.search as search
import ckan.model as model
import ckan.authz as authz
import ckan.lib.plugins
import ckan.plugins as plugins
from ckan.common import OrderedDict, c, g, request, _



# Created by Francesco Mangiacrapa
# francesco.mangiacrapa@isti.cnr.it 
# ISTI-CNR Pisa (ITALY)

class d4STypeController(base.BaseController):
    
    #Overriding controllers.HomeController.index method
    def index(self):
        try:
            # package search
            context = {'model': model, 'session': model.Session,'user': c.user, 'auth_user_obj': c.userobj}
            
            facets = OrderedDict()
            
            default_facet_titles = {
                'organization': _('Organizations'),
                'groups': _('Groups'),
                'tags': _('Tags'),
                'res_format': _('Formats'),
                'license_id': _('Licenses'),
                }
            
            for facet in g.facets:
                if facet in default_facet_titles:
                    facets[facet] = default_facet_titles[facet]
                else:
                    facets[facet] = facet
            
            # Facet titles
            for plugin in p.PluginImplementations(p.IFacets):
                facets = plugin.dataset_facets(facets, 'dataset')
            
            c.facet_titles = facets
            
            data_dict = {
                'q': '*:*',
                'facet.field': facets.keys(),
                'rows': 4,
                'start': 0,
                'sort': 'views_recent desc',
                'fq': 'capacity:"public"'
            }
            query = logic.get_action('package_search')(context, data_dict)
            c.search_facets = query['search_facets']
            c.package_count = query['count']
            c.datasets = query['results']
            
            #print "c.search_facets: "
            #print " ".join(c.search_facets)
            
        except search.SearchError:
            c.package_count = 0
        
        if c.userobj and not c.userobj.email:
            url = h.url_for(controller='user', action='edit')
            msg = _('Please <a href="%s">update your profile</a>'
            ' and add your email address. ') % url + \
            _('%s uses your email address'
            ' if you need to reset your password.') \
            % g.site_title
            h.flash_notice(msg, allow_html=True)
        
        return base.render('type/index.html', cache_force=True)

